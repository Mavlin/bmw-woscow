const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const merge = require('webpack-merge');

const pug = require('./webpack/pug');
const devserver = require('./webpack/devserver');
const extractCSS = require('./webpack/css.extract');
const cleanDist = require('./webpack/cleanDist');
const purgeCSS = require('./webpack/css.purge');
const uglifyJS = require('./webpack/js.uglify');
const images = require('./webpack/images');
const url = require('./webpack/url');
const babel = require('./webpack/babel');

console.log('process.env.NODE_ENV=',process.env.NODE_ENV);
// console.log(process.cwd())

function resolve (dir) {
    // console.log(path.join(__dirname, '.', dir))
    return path.join(__dirname, '.', dir)
}

const PATHS = {
    source: 'src',
    dist: 'dist',
    public: 'public'
};

const common = merge([
    {
        entry: {
            index: resolve(PATHS.source) + '/main.js',
        },
        output: {
            // filename: 'js/index.js',
            filename: 'js/[name].js',
            path: resolve(PATHS.dist),
            // library: '[name]'
        },
        mode: (process.env.NODE_ENV==='development')?process.env.NODE_ENV:'production',
        plugins: [
            new HtmlWebpackPlugin({
                filename: 'index.html',
                template: resolve(PATHS.source) + '/tpl/index.pug',
                // chunks: ['index', 'common'],
                options: {
                    minimize: true,
                }
            }),
            // new CopyWebpackPlugin([
            //     { from: PATHS.public },
            //     // для того, чтобы обеспечить работу сайта в среде полноценного веб сервера при
            //     // использовании template
            //     // { from: 'src/js', to: 'js', toType: 'dir' },
            // ]),
        ]
    },
    pug(),
    // images(),
    url(),
]);

let env = process.env.NODE_ENV, exp;
    if (env === 'development'){
        exp = merge([
            common,
            devserver(),
            extractCSS()
        ])
    }
    else { // default mode = 'production'
        exp = merge([
            common,
            cleanDist(PATHS.dist),
            extractCSS(),
            uglifyJS(),
            babel(),
            purgeCSS(resolve(PATHS.dist), ['active','fone-column']),
        ]);
    }

module.exports = exp;
