const path = require('path');
const PurgecssPlugin = require('purgecss-webpack-plugin');
const glob = require('glob-all');
const PurifyCSSPlugin = require('purifycss-webpack');
const fs = require('fs');

function collectWhitelistPatterns(whitelist) {
    let res = [];
    whitelist.some(function (item) {
        // console.log(item)
        res.push(new RegExp(item))
    });
    return res;
}

function collectWhitelist(whitelist) {
    let res = [];
    whitelist.some(function (item) {
        // console.log(item)
        res.push('*'+item+'*')
    });
    return res;
}
// не чистит с первого раза, требуется перезапуск
module.exports = function(dir, whitelist) {
    let source = glob.sync([
        path.join(dir, '*.html'),
        path.join(dir, '*/*.html')
    ])
    if (source.length !== 0) {
        return {
            plugins: [
                // Make sure this is after ExtractTextPlugin!
                // основной PurgeCss
                new PurifyCSSPlugin({
                    purifyOptions: {
                        whitelist: collectWhitelist(whitelist),
                        // whitelist: ['*'+whitelist+'*']
                    },
                    paths: source,
                }),
                // дополнительный PurgeCss, использует устаревшую зависимость,
                // но дополнительно чистит (10%) css
                new PurgecssPlugin({
                    whitelistPatterns: collectWhitelistPatterns(whitelist),
                    paths: source
                })
            ],
        }
    } else {
        console.log(' ------------------ reRun build for purge css -------------------')
    }
};
