module.exports = function (path) {
    return {
        module: {
            rules: [
                {
                    test: /\.pug$/,
                    // loader: 'pug-loader',
                    // options: {
                    //     pretty: true
                    // }
                    include: path,
                    use: [
                        {
                            loader:'html-loader',
                            options:{
                                minimize:true,
                            }
                        },
                        'pug-html-loader?-pretty',
                    ],
                }
            ]
        }
    }
};

